﻿namespace ConsoleApp1
{
    internal class Phone
    {
        string Number { get; set; }
        string Model { get; set; }
        int Weight { get; set; }

        public Phone() : this("N/A", "N/A", 0)
        {

        }

        public Phone(string number, string model) : this()
        {
            Number = number;
            Model = model;
        }

        public Phone(string number, string model, int weight)
        {
            Number = number;
            Model = model;
            Weight = weight;
        }

        public override string ToString() => $"Number phone: {Number}\nModel phone: {Model}\nWeight phone: {Weight} g.\n";

        public void ReceiveCall(string callersName) => Console.WriteLine($"{callersName} is calling");

        public void ReceiveCall(string callersName, string phoneNumber) => Console.WriteLine($"{phoneNumber}\r{callersName} is calling");

        public string GetNumber() => Number;

        public void SendMessage(List<string> phones) => phones.ForEach(p => Console.WriteLine($"Send message to number {p}"));

    }
}
