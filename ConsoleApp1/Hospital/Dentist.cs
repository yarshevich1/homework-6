﻿namespace ConsoleApp1.Hospital
{
    internal class Dentist : Doctor
    {
        static Random rnd = new Random();
        private string[] treatOperations = new[] { "pulls out a tooth", "makes an anesthetic injection and puts a seal", "makes an impression of the tooth", "performs an operation to remove inflammation" };

        public Dentist(string name) : base(name)
        {

        }

        public override void Treat()
        {
            Console.WriteLine($"Dentist dr.{Name} {treatOperations[rnd.Next(treatOperations.Length)]}");
        }
    }
}
