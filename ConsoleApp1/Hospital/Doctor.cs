﻿namespace ConsoleApp1.Hospital
{
    internal class Doctor
    {
        public string Name { get; set; }
        
        public Doctor(string name)
        {
            Name = name;
        }

        public virtual void Treat()
        {
            Console.WriteLine($"{Name} treats the patient.");
        }
    }
}
