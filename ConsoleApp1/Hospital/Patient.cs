﻿namespace ConsoleApp1.Hospital
{
    internal class Patient
    {
        public string Name { get; set; }
        public int TreatmentPlanCode { get; set; }

        public Patient(string name, int planCode)
        {
            Name = name;
            TreatmentPlanCode = planCode;
        }
    }
}
