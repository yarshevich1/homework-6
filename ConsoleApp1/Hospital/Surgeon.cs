﻿namespace ConsoleApp1.Hospital
{
    internal class Surgeon : Doctor
    {
        static Random rnd = new Random();
        private string[] treatOperations = new[] { "performs a surgical operation", "puts a cast on his arm", "performs a kidney transplant operation", "the knife wound is healing" };

        public Surgeon(string name) : base(name)
        {
        }

        public override void Treat()
        {
            Console.WriteLine($"Surgeon dr.{Name} {treatOperations[rnd.Next(treatOperations.Length)]}");
        }
    }
}

