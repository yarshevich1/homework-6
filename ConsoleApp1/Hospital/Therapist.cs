﻿namespace ConsoleApp1.Hospital
{
    internal class Therapist : Doctor
    {
        static Random rnd = new Random();
        private string[] treatOperations = new[] { "сonducts a survey", "prescribes a treatment plan and prescribes a prescription for pills",
            "examination of the general condition of the patient", "writes out a referral for examination" };

        public Therapist(string name) : base(name)
        {

        }

        public override void Treat()
        {
            Console.WriteLine($"Therapist dr. {Name} {treatOperations[rnd.Next(treatOperations.Length)]}");
        }
    }
}
