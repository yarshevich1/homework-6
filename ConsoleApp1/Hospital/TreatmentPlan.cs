﻿namespace ConsoleApp1.Hospital
{
    internal class TreatmentPlan
    {
        public static Doctor AssignDoctor(Patient patient)
        {
            switch (patient.TreatmentPlanCode)
            {
                case 1:
                    return new Surgeon("House");
                case 2:
                    return new Dentist("Smith");
                case 3:
                    return new Therapist("Lazy");
                default:
                    return new Therapist("Lazy");
            }
        }
    }
}
