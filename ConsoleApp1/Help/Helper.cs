﻿using System.Reflection;
using System.Text;

namespace ConsoleApp1.Help
{
    internal class Helper
    {
        static Random random = new Random();

        /// <summary>
        /// Generate new random class Phone
        /// </summary>
        /// <returns>new class Phone</returns>
        static Phone GetNewPhone() => new Phone(GeneratePhoneModel(),GeneratePhoneNumber(), GeneratePhoneWeight());

        /// <summary>
        /// Generate list random class Phone
        /// </summary>
        /// <param name="count">How time phone generate</param>
        /// <returns>list random class Phone</returns>
        public static List<Phone> GetListNewPhones(int count)
        {
            List<Phone> newPhones = new List<Phone>();

            for (int i = 0; i < count; i++)
            {
                newPhones.Add(GetNewPhone());
            }

            return newPhones;
        }

        /// <summary>
        /// Generate list random class Credit Card
        /// </summary>
        /// <param name="count">How time cc generate</param>
        /// <returns>list random class Credit Card</returns>
        public static List<CreditCard> GetListCreditCards(int count)
        {
            List<CreditCard> newCreditCards = new List<CreditCard>();

            for (int i = 0; i < count; i++)
            {
                newCreditCards.Add(new CreditCard(GenerateRandomCardNumber(), random.Next(150, 250000)));
            }

            return newCreditCards;
        }

        /// <summary>
        /// Generate random phone number
        /// </summary>
        /// <returns>random phone number</returns>
        static string GeneratePhoneNumber()
        {
            string phoneNumber = "+";
            for (int i = 0; i < 10; i++)
            {
                if (i == 1)
                    phoneNumber += "(";
                if (i == 4)
                    phoneNumber += ")";
                if (i == 7)
                    phoneNumber += "-";
                phoneNumber += random.Next(0, 10);
            }
            return phoneNumber;
        }

        /// <summary>
        /// Generate random phone model
        /// </summary>
        /// <returns>random phone model</returns>
        static string GeneratePhoneModel()
        {
            string[] brands = { "Samsung", "Apple", "Google", "Sony", "LG" };
            string[] models = { "Galaxy S21", "iPhone 13", "Pixel 6", "Xperia 5 III", "G8 ThinQ" };

            int brandIndex = random.Next(0, brands.Length);
            int modelIndex = random.Next(0, models.Length);

            return $"{brands[brandIndex]} {models[modelIndex]}";
        }

        /// <summary>
        /// Generate random phone weight
        /// </summary>
        /// <returns>random phone weight</returns>
        static int GeneratePhoneWeight() => random.Next(100, 1000);

        /// <summary>
        /// Generate random credit card
        /// </summary>
        /// <returns>random credit card</returns>
        static string GenerateRandomCardNumber()
        {
            StringBuilder cardNumber = new StringBuilder();

            for (int i = 0; i < 16; i++)
            {
                int digit = random.Next(10);
                cardNumber.Append(digit);
                if(i > 1&& i%4==0) 
                    cardNumber.Append("-");
            }

            return cardNumber.ToString();
        }

    }
}
