﻿using ConsoleApp1.Help;
using ConsoleApp1.Hospital;

namespace ConsoleApp1
{
    internal class Program
    {
        static Random rnd = new Random(DateTime.Now.Millisecond);
        static void Main(string[] args)
        {
            // Phone
            List<Phone> phones = Helper.GetListNewPhones(3);
            phones.ForEach(p => Console.WriteLine(p.ToString()));

            // Credit card
            List<CreditCard> creditCards = Helper.GetListCreditCards(3);
            creditCards.ForEach(c => c.GetAccountInfo());

            creditCards[0].SetBalance(rnd.Next(50000));
            creditCards[1].SetBalance(rnd.Next(50000));
            creditCards[2].GetBalance(rnd.Next(5000));

            creditCards.ForEach(c => c.GetAccountInfo());

            // ATM
            ATM atm = new ATM(500, 500, 500);
            atm.AddMoney(4, 5, 10);
            atm.WithdrawMoney(1500);
            atm.WithdrawMoney(5500);
            atm.WithdrawMoney(5500);

            // Hospital
            List<Patient> patient = new List<Patient>

                {
                    new Patient("Elisa", rnd.Next(1,  4)),
                    new Patient("Daniel", rnd.Next(1,  4)),
                    new Patient("Olivia", rnd.Next(1,  4))
                };

            List<Doctor> doctors = new List<Doctor>();

            foreach (var pat in patient)
            {
                Doctor doc = TreatmentPlan.AssignDoctor(pat);
                doctors.Add(doc);
                doc.Treat();
            }
        }
    }
}