﻿namespace ConsoleApp1
{
    internal class CreditCard
    {
        string AccountNumber { get; set; }
        double CurrentBalance { get; set; }

        public CreditCard() : this("N/A", 0.0)
        {
            
        }

        public CreditCard(string accountNumber, double currentBalance)
        {
            AccountNumber = accountNumber;
            CurrentBalance = currentBalance;
        }

        public void SetBalance(double amount)
        {
            if (amount > 0 && amount < double.MaxValue)
            {
                CurrentBalance += amount;
                Console.WriteLine($"You have topped up your account by ${amount:N0}\n");
            }
            else
                Console.WriteLine("You have specified an amount that is unacceptable for replenishment\n");
        }

        public void GetBalance(double amount)
        {
            if (CurrentBalance > 0 && amount <= CurrentBalance && CurrentBalance - amount > 0)
            {
                CurrentBalance -= amount;
                Console.WriteLine($"You have withdrawn ${amount:N0} from your account\n");
            }
            else
                Console.WriteLine("You have specified an amount that is unacceptable for withdrawal\n");
        }
        
        public void GetAccountInfo() => Console.WriteLine($"# You have acccount is {AccountNumber}\n# You have balance is ${CurrentBalance:N0}\n");
    }
}
