﻿using System.Diagnostics.Metrics;

namespace ConsoleApp1
{
    internal class ATM
    {
        private int TwentyCount { get; set; }
        private int FiftyCount { get; set; }
        private int OneHundredCount { get; set; }
        private int TotalAmount { get; set; }


        public ATM(int twentyCount, int fiftyCount, int oneHundredCount)
        {
            TwentyCount = twentyCount;
            FiftyCount = fiftyCount;
            OneHundredCount = oneHundredCount;
            TotalAmount = TwentyCount * 20 + FiftyCount * 50 + OneHundredCount * 100;
        }

        public void AddMoney(int twentyCount, int fiftyCount, int oneHundredCount)
        {
            TwentyCount += twentyCount;
            FiftyCount += fiftyCount;
            OneHundredCount = oneHundredCount;
            TotalAmount += twentyCount * 20 + fiftyCount * 50 + oneHundredCount * 100;
        }

        public bool WithdrawMoney(int amountToWithdraw)
        {
            if (amountToWithdraw > TotalAmount)
            {
                Console.WriteLine("There are not enough funds in the ATM.");
                return false;
            }

            if (amountToWithdraw % 10 != 0)
            {
                Console.WriteLine("The amount must be a multiple of 10.");
                return false;
            }


            int remainingAmount = amountToWithdraw;

            int withdrawnHundred = Math.Min(remainingAmount / 100, OneHundredCount);
            remainingAmount -= withdrawnHundred * 100;

            int withdrawnFifty = Math.Min(remainingAmount / 50, FiftyCount);
            remainingAmount -= withdrawnFifty * 50;

            int withdrawnTwenty = Math.Min(remainingAmount / 20, TwentyCount);
            remainingAmount -= withdrawnTwenty * 20;

            if (remainingAmount != 0)
            {
                Console.WriteLine("It is impossible to issue the specified amount.");
                return false;
            }

            OneHundredCount -= withdrawnHundred;
            FiftyCount -= withdrawnFifty;
            TwentyCount -= withdrawnTwenty;

            Console.WriteLine($"The amount {amountToWithdraw} has been successfully issued:\n100 banknotes: {withdrawnHundred}\n50 banknotes: {withdrawnFifty}\n20 banknotes: {withdrawnTwenty}");

            return true;
        }

    }
}
